import React, { Component } from "react";
import PropTypes from "prop-types";
import SaveButton from "./SaveButton";

class MyPage extends Component {
  render() {
    return (
      <div>
        {/* other elements */}
        <SaveButton/>
      </div>
    );
  }
}

export default MyPage;