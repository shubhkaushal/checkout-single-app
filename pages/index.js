import React, { Component } from "react";
import PropTypes from "prop-types";
import { ComponentsProvider } from "@reactioncommerce/components-context";
import appComponents from "./appComponents";
import MyPage from "./MyPage";

class App extends Component {
  render() {
    return (
      <ComponentsProvider value={appComponents}>
        <MyPage />
      </ComponentsProvider>
    );
  }
}

export default App;